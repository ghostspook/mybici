package com.intelroad.mybici;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.intelroad.mybici.bizobjs.Ticket;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReservaConfirmacionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReservaConfirmacionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReservaConfirmacionFragment extends Fragment {
    private static final String TICKET_ID = "id";
    private static final String BICICLETA_CODIGO = "bicicletaCodigo";
    private static final String FECHA = "fecha";
    private static final String ESTACION_ORIGEN_NOMBRE = "estacionOrigenNombre";
    private static final String ESTACION_DESTINO_NOMBRE = "estacionDestinoNombre";
    private static final String HORA_CREACION = "horaCreacion";
    private static final String HORA_LIMITE_RETIRO = "horaLimiteRetiro";
    private static final String PARQUEO_ORIGEN_CODIGO = "parqueoOrigenCodigo";
    private static final String TAG = Fragment.class.getSimpleName();

    private int mTicketId;
    private String mBicicletaCodigo;
    private String mFecha;
    private String mEstacionOrigenNombre;
    private String mEstacionDestinoNombre;
    private String mHoraCreacion;
    private String mHoraLimiteRetiro;
    private String mParqueoOrigenCodigo;

    private OnFragmentInteractionListener mListener;

    public ReservaConfirmacionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param t Ticket de confirmación.
     * @return A new instance of fragment ReservaConfirmacionFragment.
     */
    public static ReservaConfirmacionFragment newInstance(Ticket t) {
        ReservaConfirmacionFragment fragment = new ReservaConfirmacionFragment();
        Bundle args = new Bundle();
        Log.d(TAG, "Ticket: " + t.getId());
        args.putInt(TICKET_ID, t.getId());
        args.putString(BICICLETA_CODIGO, t.getBicicletaCodigo());
        args.putString(FECHA, t.getFecha());
        args.putString(ESTACION_ORIGEN_NOMBRE, t.getEstacionOrigenNombre());
        args.putString(ESTACION_DESTINO_NOMBRE, t.getEstacionDestinoNombre());
        args.putString(HORA_CREACION, t.getHoraCreacion());
        args.putString(HORA_LIMITE_RETIRO, t.getHoraLimiteRetiro());
        args.putString(PARQUEO_ORIGEN_CODIGO, t.getParqueoOrigenCodigo());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTicketId = getArguments().getInt(TICKET_ID);
            mBicicletaCodigo = getArguments().getString(BICICLETA_CODIGO);
            mFecha = getArguments().getString(FECHA);
            mEstacionOrigenNombre = getArguments().getString(ESTACION_ORIGEN_NOMBRE);
            mEstacionDestinoNombre = getArguments().getString(ESTACION_DESTINO_NOMBRE);
            mHoraCreacion = getArguments().getString(HORA_CREACION);
            mHoraLimiteRetiro = getArguments().getString(HORA_LIMITE_RETIRO);
            mParqueoOrigenCodigo = getArguments().getString(PARQUEO_ORIGEN_CODIGO);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_reserva_confirmacion, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        TextView ticketIdView = (TextView) this.getActivity().findViewById(R.id.textView);
        ticketIdView.setText("Ticket No. " + mTicketId);
        TextView fechaTextView = (TextView) this.getActivity().findViewById(
                R.id.fecha_confirmacion_text);
        fechaTextView.setText(mFecha);
        TextView bicicletaTextView = (TextView) this.getActivity().findViewById(
                R.id.bicicleta_text);
        bicicletaTextView.setText(mBicicletaCodigo);
        TextView estacionOrigenTextView = (TextView) this.getActivity().findViewById(
                R.id.origen_cofirmacion_text);
        estacionOrigenTextView.setText(mEstacionOrigenNombre);
        TextView estacionDestinoTextView = (TextView) this.getActivity().findViewById(
                R.id.destino_confirmacion_text);
        estacionDestinoTextView.setText(mEstacionDestinoNombre);
        TextView horaGeneradoTextView = (TextView) this.getActivity().findViewById(
                R.id.confirmacion_hora_generado_text);
        horaGeneradoTextView.setText(mHoraCreacion);
        TextView horaLimiteRetiroTextView = (TextView) this.getActivity().findViewById(
                R.id.confirmacion_hora_limite_retiro);
        horaLimiteRetiroTextView.setText(mHoraLimiteRetiro);
        TextView parqueoOrigenTextView = (TextView) this.getActivity().findViewById(
                R.id.parqueo_estacion_origen);
        parqueoOrigenTextView.setText(mParqueoOrigenCodigo);

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public int getTicketId() {
        return mTicketId;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
