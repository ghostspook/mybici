package com.intelroad.mybici.bizobjs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by rafaelcastillo on 23/1/16.
 */
public class Ticket {

    private int id;
    private int tipoId;
    private String tipoTexto = "";
    private int usuarioId;
    private String usuarioNombre = "";
    private int bicicletaId;
    private String bicicletaCodigo = "";
    private int estacionOrigenId;
    private String estacionOrigenNombre = "";
    private int estacionOrigenParqueoId;
    private int estacionDestinoId;
    private String estacionDestinoNombre = "";
    private int estacionDestinoParqueoId;
    private String fecha = "";
    private String horaCreacion;
    private String horaLimiteRetiro;
    private String horaRetiro = "";
    private String horaEntrega = "";
    private int estadoId;
    private String parqueoOrigenCodigo="";

    public static Ticket getTicket(JSONObject jObject) throws JSONException {
        Ticket obj = new Ticket();
        obj.id = jObject.getInt("id");
        obj.tipoId = jObject.getInt("tipoId");
        //obj.tipoTexto = jObject.getString("tipoTexto");
        obj.usuarioId = jObject.getInt("usuarioId");
        //obj.usuarioNombre = jObject.getString("usuarioNombre");
        obj.bicicletaId = jObject.getInt("bicicletaId");
        obj.bicicletaCodigo = jObject.getString("bicicletaCodigo");
        obj.estacionOrigenId = jObject.getInt("estacionOrigenId");
        obj.estacionOrigenNombre = jObject.getString("estacionOrigenNombre");
        obj.estacionOrigenParqueoId = jObject.getInt("estacionOrigenParqueoId");
        obj.estacionDestinoId = jObject.getInt("estacionDestinoId");
        obj.estacionDestinoNombre = jObject.getString("estacionDestinoNombre");
        if (!jObject.isNull("estacionDestinoParqueoId")) {
            obj.estacionDestinoParqueoId = jObject.getInt("estacionDestinoParqueoId");
        } else { obj.estacionDestinoParqueoId = 0; }
        obj.fecha = jObject.getString("fecha");
        obj.horaCreacion = jObject.getString("horaCreacion");
        obj.horaLimiteRetiro = jObject.getString("horaLimiteRetiro");
        obj.horaRetiro = jObject.getString("horaRetiro");
        obj.horaEntrega = jObject.getString("horaEntrega");
        obj.estadoId = jObject.getInt("estadoId");
        obj.parqueoOrigenCodigo = jObject.getString("parqueoOrigenCodigo");
        return obj;
    }

    public int getId() {
        return id;
    }

    public int getTipoId() {
        return tipoId;
    }

    public String getTipoTexto() {
        return tipoTexto;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public String getUsuarioNombre() {
        return usuarioNombre;
    }

    public int getBicicletaId() {
        return bicicletaId;
    }

    public String getBicicletaCodigo() {
        return bicicletaCodigo;
    }

    public int getEstacionOrigenId() {
        return estacionOrigenId;
    }

    public String getEstacionOrigenNombre() {
        return estacionOrigenNombre;
    }

    public int getEstacionOrigenParqueoId() {
        return estacionOrigenParqueoId;
    }

    public int getEstacionDestinoId() {
        return estacionDestinoId;
    }

    public String getEstacionDestinoNombre() {
        return estacionDestinoNombre;
    }

    public int getEstacionDestinoParqueoId() {
        return estacionDestinoParqueoId;
    }

    public String getFecha() {
        return fecha;
    }

    public String getHoraRetiro() {
        return horaRetiro;
    }

    public String getHoraEntrega() {
        return horaEntrega;
    }

    public int getEstadoId() {
        return estadoId;
    }

    public String getEstadoTexto() {
        switch (estadoId) {
            case 10:
                return "Generado";
            case 11:
                return "En curso";
            case 12:
                return "Realizado";
            case 13:
                return "Anulado";
            default:
                return "?";
        }
    }

    public String getHoraCreacion() {
        return horaCreacion;
    }

    public String getHoraLimiteRetiro() {
        return horaLimiteRetiro;
    }

    public String getParqueoOrigenCodigo() {
        return parqueoOrigenCodigo;
    }
}
