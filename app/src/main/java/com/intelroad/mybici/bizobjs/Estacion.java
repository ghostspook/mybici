package com.intelroad.mybici.bizobjs;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by rafaelcastillo on 22/1/16.
 */
public class Estacion {
    private int id;
    private String codigo;
    private double latitud;
    private double longitud;
    private String nombre;
    private int parqueosLlenosCount;
    private int parqueosVaciosCount;
    private int bicicletasDisponiblesCount;

    public static Estacion getEstacion(JSONObject jObject) throws JSONException{
        Estacion obj = new Estacion();
        obj.id = jObject.getInt("id");
        obj.codigo = jObject.getString("id");
        obj.latitud = jObject.getDouble("latitud");
        obj.longitud = jObject.getDouble("longitud");
        obj.nombre = jObject.getString("nombre");
        obj.parqueosLlenosCount = jObject.getInt("parqueosLlenosCount");
        obj.parqueosVaciosCount = jObject.getInt("parqueosVaciosCount");
        obj.bicicletasDisponiblesCount = jObject.getInt("bicicletasDisponiblesCount");
        return obj;
    }

    public int getId() {
        return id;
    }

    public String getCodigo() {
        return codigo;
    }

    public double getLatitud() {
        return latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public String getNombre() {
        return nombre;
    }

    public int getParqueosLlenosCount() {
        return parqueosLlenosCount;
    }

    public int getParqueosVaciosCount() {
        return parqueosVaciosCount;
    }

    public int getBicicletasDisponiblesCount() {
        return bicicletasDisponiblesCount;
    }
}
