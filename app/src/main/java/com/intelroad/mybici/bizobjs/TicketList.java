package com.intelroad.mybici.bizobjs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by rafaelcastillo on 24/1/16.
 */
public class TicketList extends ArrayList<Ticket> {

    public static TicketList getTicketList(JSONObject jObject)throws JSONException {
        TicketList obj = new TicketList();
        Iterator<?> keys = jObject.keys();
        while(keys.hasNext() ) {
            String key = (String)keys.next();
            if ( jObject.get(key) instanceof JSONObject ) {
                JSONObject item = new JSONObject(jObject.get(key).toString());
                obj.add(Ticket.getTicket(item));
            }
        }
        return obj;
    }
}
