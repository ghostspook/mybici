package com.intelroad.mybici.bizobjs;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by rafaelcastillo on 22/1/16.
 */
public class EstacionList extends ArrayList<Estacion> {

    public static EstacionList getEstacionList(JSONObject jObject)throws JSONException{
        EstacionList obj = new EstacionList();
        Iterator<?> keys = jObject.keys();
        while(keys.hasNext() ) {
            String key = (String)keys.next();
            if ( jObject.get(key) instanceof JSONObject ) {
                JSONObject item = new JSONObject(jObject.get(key).toString());
                obj.add(Estacion.getEstacion(item));
            }
        }
        return obj;
    }

    public Estacion getByNombre(String nombre) {
        for (Estacion est: this
             ) {
            if (est.getNombre().equals(nombre)) {
                return est;
            }
        }
        return null;
    }
}
