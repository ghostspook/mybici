package com.intelroad.mybici;

import com.intelroad.mybici.bizobjs.EstacionList;
import com.intelroad.mybici.bizobjs.Ticket;
import com.intelroad.mybici.bizobjs.TicketList;

/**
 * Created by rafaelcastillo on 17/1/16.
 */
public class DataHolder {
    private static String token;
    public static String getToken() {return token;}
    public static void setToken(String token) {DataHolder.token = token;}

    private static String username;
    public static String getUsername() {return username;}
    public static void setUsername(String username) {DataHolder.username = username;}

    private static EstacionList estaciones;
    public static EstacionList getEstaciones() {
        return estaciones;
    }

    public static void setEstaciones(EstacionList estaciones) {
        DataHolder.estaciones = estaciones;
    }

    private static TicketList misTickets;


    public static TicketList getMisTickets() {
        return misTickets;
    }

    public static void setMisTickets(TicketList misTickets) {
        DataHolder.misTickets = misTickets;
    }

    public static Ticket ticketSeleccionado;

    public static Ticket getTicketSeleccionado() {
        return ticketSeleccionado;
    }

    public static void setTicketSeleccionado(Ticket ticketSeleccionado) {
        DataHolder.ticketSeleccionado = ticketSeleccionado;
    }



}