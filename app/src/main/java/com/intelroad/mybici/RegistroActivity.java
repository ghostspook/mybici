package com.intelroad.mybici;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.intelroad.mybici.bizobjs.EstacionList;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

public class RegistroActivity extends AppCompatActivity {

    private static final String TAG = RegistroActivity.class.getSimpleName();
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserRegistrationTask mRegistrationTask = null;

    // UI references.
    private EditText mUsernameView;
    private EditText mPasswordView;
    private EditText mConfirmPasswordView;
    private View mProgressView;
    private View mRegistrationFormView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set up the login form.
        mUsernameView = (EditText) findViewById(R.id.username_registro);

        mPasswordView = (EditText) findViewById(R.id.password_registro);
        mConfirmPasswordView = (EditText) findViewById(R.id.password_confirmacion);

        Button mEmailSignInButton = (Button) findViewById(R.id.guardar_button);
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptRegistration();
            }
        });

        mRegistrationFormView = findViewById(R.id.registro_form);
        mProgressView = findViewById(R.id.registro_progress);
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptRegistration() {
        if (mRegistrationTask != null) {
            return;
        }

        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();
        String confirmPassword = mConfirmPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        } else  if (!password.equals(confirmPassword)) {
            mConfirmPasswordView.setError(("Contraseñas no coinciden"));
            focusView = mConfirmPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } else if (!isUsernameValid(username)) {
            mUsernameView.setError(getString(R.string.error_invalid_email));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            //showProgress(true);
            mRegistrationTask = new UserRegistrationTask(username, password);
            mRegistrationTask.execute((Void) null);
        }
    }

    private boolean isUsernameValid(String email) {
        return (email.length() > 0);
    }

    private boolean isPasswordValid(String password) {
        boolean lengthValid =  password.length() > 7;
        return lengthValid;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mRegistrationFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mRegistrationFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public class UserRegistrationTask extends AsyncTask<Void, Void, Boolean> {

        private final String mUsername;
        private final String mPassword;
        private String mToken;
        private EstacionList mEstaciones;
        private String mErrorMessage;

        UserRegistrationTask(String email, String password) {
            mUsername = email;
            mPassword = password;
            mErrorMessage = "";
        }

        public String getToken() {
            return mToken;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                if (!isCancelled()){
                    Log.d(TAG, "About to invoke service @ " + Params.RESTServiceURL + "/registro");
                    String result = invokeRegistroService(Params.RESTServiceURL + "/registro",
                            mUsername, mPassword);
                    Log.d(TAG, "Result: " + result);

                    JSONObject jObject = new JSONObject(result);
                    if (jObject.getString("resultado").equals("ok")) {
                        mToken = jObject.getString("token");
                        Log.d(TAG, "Token: " + mToken);

                        Log.d(TAG, "About to invoke service @ " + Params.RESTServiceURL +
                                "/estaciones");
                        String result2 = invokeEstacionesService(Params.RESTServiceURL +
                                "/estaciones", mToken);
                        Log.d(TAG, "Result: " + result2);

                        JSONObject jObject2 = new JSONObject(result2);
                        mEstaciones = EstacionList.getEstacionList(jObject2);

                        return true;
                    } else if (jObject.getString("error").equals("1")) {
                        Log.d(TAG, "Usuario ya existe");
                        mErrorMessage = "Usuario ya existe";
                        return false;
                    } else {
                        Log.d(TAG, "No se pudo interpretar la respuesta del servidor.");
                        mErrorMessage = "Error: Comuníquese con el administrador";
                        return false;
                    }
                } else {
                    return false;
                }

            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
                return false;
            }

        }

        private String invokeEstacionesService(String myurl, String token)
                throws IOException {
            InputStream is = null;
            int length = 4000;

            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoOutput(false);
                conn.setDoInput(true);

                conn.connect();
                Log.d(TAG, "Connection opened");
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is: " + response);
                is = conn.getInputStream();

                // Convert the InputStream into a string
                return convertInputStreamToString(is, length);
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        private String invokeRegistroService(String myurl, String username, String password)
                throws IOException {
            InputStream is = null;
            int length = 500;

            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                Log.d(TAG, "Connection created");
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
                conn.setDoInput(true);

                String str =  "{\"user\": \"" + username + "\",\"password\":\"" + password + "\"}";
                Log.d(TAG, str);
                byte[] outputInBytes = str.getBytes("UTF-8");
                OutputStream os = conn.getOutputStream();
                os.write(outputInBytes);
                os.close();


                conn.connect();
                Log.d(TAG, "Connection opened");
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is: " + response);
                is = conn.getInputStream();

                // Convert the InputStream into a string
                return convertInputStreamToString(is, length);
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        public String convertInputStreamToString(InputStream stream, int length)
                throws IOException, UnsupportedEncodingException {
            Reader reader = null;
            reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[length];
            reader.read(buffer);
            return new String(buffer);
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mRegistrationTask = null;
            showProgress(false);

            if (success) {
                SharedPreferences.Editor editor = getSharedPreferences(
                        "com.intelroad.mybici", MODE_PRIVATE).edit();
                editor.putString("token", mToken);
                editor.commit();
                DataHolder.setToken(mToken);
                DataHolder.setUsername(mUsername);
                DataHolder.setEstaciones(mEstaciones);
                Toast.makeText(RegistroActivity.this, "Usuario creado exitosamente",
                        Toast.LENGTH_LONG).show();

                Intent i = new Intent(RegistroActivity.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            } else {
                Toast.makeText(RegistroActivity.this, mErrorMessage,
                        Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            mRegistrationTask = null;
            showProgress(false);
        }
    }

}
