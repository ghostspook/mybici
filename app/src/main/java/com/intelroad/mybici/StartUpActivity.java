package com.intelroad.mybici;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.intelroad.mybici.bizobjs.EstacionList;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

public class StartUpActivity extends AppCompatActivity {

    private static final String TAG = StartUpActivity.class.getSimpleName();
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private TokenLoginTask mTokenAuthTask = null;
    private View mProgressView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_up);

        mProgressView = findViewById(R.id.token_login_progress);
        final RelativeLayout layout = (RelativeLayout)findViewById(R.id.start_up_layout);
        ViewTreeObserver vto = layout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Log.d(TAG, "Layout de inicio completo");
                AfterLayoutDone();
                layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    private void AfterLayoutDone() {
        String token = readToken();
        Log.d(TAG, "Preferncias leídas");
        if (token != null) {
            Log.d(TAG, "token: " + token);
            attemptTokenLogin(token);
        } else {
            Log.d(TAG, "No token stored");
            Intent i = new Intent(StartUpActivity.this, LoginActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            finish();
        }
    }

    private String readToken() {
        SharedPreferences prefs = getSharedPreferences(
                "com.intelroad.mybici", MODE_PRIVATE);
        return prefs.getString("token", null);
    }

    private void attemptTokenLogin(String token) {
        if (mTokenAuthTask != null) {
            return;
        }

        boolean cancel = false;
        View focusView = null;


        if (cancel) {
            finish();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mTokenAuthTask = new TokenLoginTask(token);
            mTokenAuthTask.execute((Void) null);
        }
    }

    /**
     * Shows the progress UI
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    public class TokenLoginTask extends AsyncTask<Void, Void, Boolean> {

        private String mUsername;
        private final String mToken;
        private EstacionList mEstaciones;
        private boolean socketTimeout = false;

        TokenLoginTask(String token) {
            mUsername = "";
            mToken = token;
        }

        public String getUsername() {
            return mUsername;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                if (!isCancelled()){
                    Log.d(TAG, "About to invoke service @ " + Params.RESTServiceURL + "/token");
                    String result = invokeLoginService(Params.RESTServiceURL + "/token",
                            mToken);
                    Log.d(TAG, "Result: " + result);

                    JSONObject jObject = new JSONObject(result);
                    if (jObject.getString("resultado").equals("ok")) {
                        mUsername = jObject.getString("user");
                        Log.d(TAG, "User: " + mUsername);

                        Log.d(TAG, "About to invoke service @ " + Params.RESTServiceURL +
                                "/estaciones");
                        String result2 = invokeEstacionesService(Params.RESTServiceURL +
                                "/estaciones", mToken);
                        Log.d(TAG, "Result: " + result2);

                        JSONObject jObject2 = new JSONObject(result2);
                        mEstaciones = EstacionList.getEstacionList(jObject2);

                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }


            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
                return false;
            }


        }

        private String invokeEstacionesService(String myurl, String token)
                throws IOException {
            InputStream is = null;
            int length = 4000;

            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoOutput(false);
                conn.setDoInput(true);

                conn.connect();
                Log.d(TAG, "Connection opened");
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is: " + response);
                is = conn.getInputStream();

                // Convert the InputStream into a string
                return convertInputStreamToString(is, length);
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        private String invokeLoginService(String myurl, String token)
                throws IOException {
            InputStream is = null;
            int length = 500;

            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
                conn.setDoInput(true);

                String str =  "{\"token\": \"" + token + "\"}";
                Log.d(TAG, str);
                byte[] outputInBytes = str.getBytes("UTF-8");
                OutputStream os = conn.getOutputStream();
                os.write(outputInBytes);
                os.close();


                conn.connect();
                Log.d(TAG, "Connection opened");
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is: " + response);
                is = conn.getInputStream();

                // Convert the InputStream into a string
                return convertInputStreamToString(is, length);
            } catch (java.net.SocketTimeoutException e) {
                socketTimeout = true;
                return "";
            }
            finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        public String convertInputStreamToString(InputStream stream, int length)
                throws IOException, UnsupportedEncodingException {
            Reader reader = null;
            reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[length];
            reader.read(buffer);
            return new String(buffer);
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mTokenAuthTask = null;

            showProgress(false);

            if (success) {
                DataHolder.setToken(mToken);
                DataHolder.setUsername(mUsername);
                DataHolder.setEstaciones(mEstaciones);
                Intent i = new Intent(StartUpActivity.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            } else if (socketTimeout) {
                Toast.makeText(StartUpActivity.this, "Problema de conexión al servidor",
                        Toast.LENGTH_LONG).show();
                finish();
            }
            else {
                Intent i = new Intent(StartUpActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
            }
        }

        @Override
        protected void onCancelled() {
            mTokenAuthTask = null;
            showProgress(false);
        }
    }


}
