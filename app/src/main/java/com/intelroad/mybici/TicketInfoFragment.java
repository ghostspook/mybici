package com.intelroad.mybici;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.intelroad.mybici.bizobjs.Ticket;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TicketInfoFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TicketInfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TicketInfoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public TicketInfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TicketInfoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TicketInfoFragment newInstance(String param1, String param2) {
        TicketInfoFragment fragment = new TicketInfoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ticket_info, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Ticket t = DataHolder.getTicketSeleccionado();
        TextView ticketNo = (TextView) this.getActivity().findViewById(
                R.id.ticket_no_info);
        ticketNo.setText("Ticket No. " + t.getId());
        TextView estadoTexto = (TextView) this.getActivity().findViewById(
                R.id.ticket_estado);
        estadoTexto.setText(t.getEstadoTexto());
        TextView bicicletaCodigo = (TextView) this.getActivity().findViewById(
                R.id.bicicleta_codigo_ticket);
        bicicletaCodigo.setText(t.getBicicletaCodigo());
        TextView fecha = (TextView) this.getActivity().findViewById(
                R.id.fecha_ticket);
        fecha.setText(t.getFecha());
        TextView hora = (TextView) this.getActivity().findViewById(
                R.id.hora_ticket);
        hora.setText(t.getHoraCreacion());
        TextView origen = (TextView) this.getActivity().findViewById(
                R.id.origen_ticket);
        origen.setText(t.getEstacionOrigenNombre());
        TextView destino = (TextView) this.getActivity().findViewById(
                R.id.destino_ticket);
        destino.setText(t.getEstacionDestinoNombre());
        Button anularRegenerarButton = (Button) this.getActivity().findViewById(
                R.id.anular_regenerar_ticket_button);
        TextView labelHoraLimiteRetiro = (TextView) this.getActivity().findViewById(
                R.id.label_hora_limite_retiro_ticket);
        TextView horaLimiteRetiro = (TextView) this.getActivity().findViewById(
                R.id.hora_limite_retiro_ticket);
        TextView parqueoOrigenLabel = (TextView) this.getActivity().findViewById(
                R.id.label_parqueo_origen);
        TextView parqueoOrigen = (TextView) this.getActivity().findViewById(
                R.id.parqueo_origen);

        if (t.getEstadoId() == 10) {
            horaLimiteRetiro.setText(t.getHoraLimiteRetiro());
            anularRegenerarButton.setText("Anular");
            parqueoOrigen.setText(t.getParqueoOrigenCodigo());
        } else if (t.getEstadoId() == 11) {
            labelHoraLimiteRetiro.setVisibility(View.INVISIBLE);
            horaLimiteRetiro.setVisibility(View.INVISIBLE);
            parqueoOrigenLabel.setVisibility(View.INVISIBLE);
            parqueoOrigen.setVisibility(View.INVISIBLE);
            anularRegenerarButton.setText("Volver a generar");
            anularRegenerarButton.setEnabled(false);
        } else {
            labelHoraLimiteRetiro.setVisibility(View.INVISIBLE);
            horaLimiteRetiro.setVisibility(View.INVISIBLE);
            parqueoOrigenLabel.setVisibility(View.INVISIBLE);
            parqueoOrigen.setVisibility(View.INVISIBLE);
            anularRegenerarButton.setText("Volver a generar");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
