package com.intelroad.mybici;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import android.widget.Toast;

import com.intelroad.mybici.bizobjs.Ticket;
import com.intelroad.mybici.bizobjs.TicketList;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        TicketsFragment.OnFragmentInteractionListener,
        EstacionesFragment.OnFragmentInteractionListener,
        ReservasFragment.OnFragmentInteractionListener,
        TicketInfoFragment.OnFragmentInteractionListener,
        ReservaConfirmacionFragment.OnFragmentInteractionListener{

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String RESERVAS_FRAGMENT = "ReservasFragment";
    private static final String TICKETS_FRAGMENT = "TicketsFragment";
    private static final String RESERVA_CONFIRMACION_FRAGMENT = "ReservaConfirmacionFragment";

    private ReservaTask mReservaTask = null;
    private MisTicketsTask mMisTicketsTask = null;
    private AnularReservaTask mAnularReservaTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final DrawerLayout layout = (DrawerLayout)findViewById(R.id.drawer_layout);
        ViewTreeObserver vto = layout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Log.d(TAG, "Layout de barra de navegaciòn completo");
                AfterLayoutDone();
                layout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                goToEstaciones();
            }
        });
    }

    public void AfterLayoutDone() {
        TextView username = (TextView) findViewById(R.id.ui_username);
        username.setText("Bienvenido(a), " + DataHolder.getUsername());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle Reservas view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_reservas) {
            Fragment reservasFragment = new ReservasFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, reservasFragment, RESERVAS_FRAGMENT)
                    .commit();
        } else if (id == R.id.nav_tickets) {
            goToMisTickets();
        } else if (id == R.id.nav_estaciones) {
            goToEstaciones();
        } else if (id == R.id.nav_cerrar_sesion) {
            final AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
            dlgAlert.setMessage("¿Desea salir?");
            dlgAlert.setTitle("MyBici");
            dlgAlert.setPositiveButton("Sí",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences.Editor editor = getSharedPreferences(
                                    "com.intelroad.mybici", MODE_PRIVATE).edit();
                            editor.remove("token");
                            editor.commit();
                            Intent i = new Intent(MainActivity.this, LoginActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(i);
                            finish();
                        }
                    });
            dlgAlert.setCancelable(true);
            dlgAlert.setNegativeButton("No", null);
            dlgAlert.create().show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void goToEstaciones() {
        Log.d(TAG, "Creando nuevo fragmento");
        Fragment estacionesFragment = new EstacionesFragment();
        Log.d(TAG, "Reemplazando fragmento actual");

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, estacionesFragment)
                .commit();
    }

    public void goToMisTickets() {
        mMisTicketsTask = new MisTicketsTask(DataHolder.getUsername(), this);
        mMisTicketsTask.execute();
    }

    public void onClickCancelarReserva(View v) {
        goToEstaciones();
    }

    public void onClickContinuarReserva(View v) {
        if (mReservaTask != null) {
            return;
        }

        boolean cancel = false;
        View focusView = null;


        if (cancel) {
            finish();
        } else {
            //showProgress(true);
            v.findViewById(R.id.origen_text);
            ReservasFragment f = (ReservasFragment) getSupportFragmentManager().
                    findFragmentByTag(RESERVAS_FRAGMENT);
            Log.d(TAG, "Estación Origen " + f.getEstacionOrigen().getNombre());
            Log.d(TAG, "Estación Destino " + f.getEstacionDestino().getNombre());
            mReservaTask = new ReservaTask(DataHolder.getUsername(),
                    f.getEstacionOrigen().getId(), f.getEstacionDestino().getId());
            mReservaTask.execute((Void) null);
        }
    }

    public void onClickRegresarDeTicketInfo(View v) {
        TicketsFragment ticketsFragment = new TicketsFragment();
        ticketsFragment.setListener(this);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, ticketsFragment, TICKETS_FRAGMENT)
                .commit();
    }

    public void onClickAnularReserva(View v) {
        final AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage("¿Desea anular la reserva?");
        dlgAlert.setTitle("MyBici");
        dlgAlert.setPositiveButton("Sí",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ReservaConfirmacionFragment f = (ReservaConfirmacionFragment) getSupportFragmentManager().
                                findFragmentByTag(RESERVA_CONFIRMACION_FRAGMENT);
                        mAnularReservaTask = new AnularReservaTask(f.getTicketId());
                        mAnularReservaTask.execute();
                    }
                });
        dlgAlert.setCancelable(true);
        dlgAlert.setNegativeButton("No", null);
        dlgAlert.create().show();
    }

    public void onClickAnularRegenerar(View v) {
        final AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        final Ticket t = DataHolder.getTicketSeleccionado();
        final MainActivity mainActivity = (MainActivity) this;

        if (t.getEstadoId() == 10) {
            dlgAlert.setMessage("¿Desea anular la reserva?");
            dlgAlert.setTitle("MyBici");

            dlgAlert.setPositiveButton("Sí",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            mAnularReservaTask = new AnularReservaTask(t.getId(), mainActivity);
                            mAnularReservaTask.execute();
                        }
                    });
            dlgAlert.setCancelable(true);
            dlgAlert.setNegativeButton("No", null);
            dlgAlert.create().show();
        } else if(t.getEstadoId() == 12) {
            mReservaTask = new ReservaTask(DataHolder.getUsername(),
                    t.getEstacionOrigenId(), t.getEstacionDestinoId());
            mReservaTask.execute();
        }


    }

    @Override
    public void onTicketSeleccionado(Ticket t) {
        Fragment f = new TicketInfoFragment();
        DataHolder.setTicketSeleccionado(t);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, f)
                .commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public class ReservaTask extends AsyncTask<Void, Void, Boolean> {

        private Ticket mTicket;
        private String mUsername;
        private int mEstacionOrigenId;
        private int mEstacionDestinoId;
        private String mErrorMessage = "";

        ReservaTask(String username, int estacionOrigenId, int estacionDestinoId) {
            mUsername = username;
            mEstacionOrigenId = estacionOrigenId;
            mEstacionDestinoId = estacionDestinoId;
        }

        public String getErrorMessage() {
            return mErrorMessage;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                if (!isCancelled()){
                    Log.d(TAG, "About to invoke service @ " + Params.RESTServiceURL +
                            "/tickets/reserva");
                    String result = invokeReservaService(Params.RESTServiceURL + "/tickets/reserva");
                    Log.d(TAG, "Result: " + result);

                    JSONObject jObject = new JSONObject(result);

                    if (!jObject.has("error")) {
                        mTicket = Ticket.getTicket(jObject);

                        return true;
                    } else {
                        mErrorMessage = jObject.getString("message");
                        return false;
                    }
                } else {
                    return false;
                }


            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
                return false;
            }


        }

        private String invokeReservaService(String myurl)
                throws IOException, JSONException {
            InputStream is = null;
            int length = 500;

            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
                conn.setDoInput(true);

                JSONObject jObj = new JSONObject();
                jObj.put("user", mUsername);
                jObj.put("estacionOrigenId", mEstacionOrigenId);
                jObj.put("estacionDestinoId", mEstacionDestinoId);
                Log.d(TAG, jObj.toString());
                byte[] outputInBytes = jObj.toString().getBytes("UTF-8");
                OutputStream os = conn.getOutputStream();
                os.write(outputInBytes);
                os.close();

                conn.connect();
                Log.d(TAG, "Connection opened");
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is: " + response);
                is = conn.getInputStream();

                // Convert the InputStream into a string
                return convertInputStreamToString(is, length);
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }


        public String convertInputStreamToString(InputStream stream, int length)
                throws IOException  {
            Reader reader;
            reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[length];
            reader.read(buffer);

            return new String(buffer);
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mReservaTask = null;

            //showProgress(false);
            if (success) {
                Fragment f = ReservaConfirmacionFragment.newInstance(mTicket);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, f, RESERVA_CONFIRMACION_FRAGMENT)
                        .commit();

            } else if (mErrorMessage.length() > 0) {
                Toast.makeText(MainActivity.this, mErrorMessage,
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(MainActivity.this, "No pudo generarse el ticket",
                        Toast.LENGTH_LONG).show();
            }


        }

        @Override
        protected void onCancelled() {
            mReservaTask = null;
            //showProgress(false);
        }
    }

    public class AnularReservaTask extends AsyncTask<Void, Void, Boolean> {

        private MainActivity mMainActivityObj;
        private int mTicketId;

        AnularReservaTask(int ticketId) {
            mTicketId = ticketId;
        }

        AnularReservaTask(int ticketId, MainActivity obj) {
            mTicketId = ticketId;
            mMainActivityObj = obj;
        }


        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                if (!isCancelled()){
                    Log.d(TAG, "About to invoke service @ " + Params.RESTServiceURL +
                            "/tickets/anular/" + mTicketId);
                    String result = invokeAnularReservaService(
                            Params.RESTServiceURL + "/tickets/anular/" + mTicketId);
                    Log.d(TAG, "Result: " + result);

                    JSONObject jObject = new JSONObject(result);

                    if (jObject.has("status") && jObject.get("status").equals("1")) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }


            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
                return false;
            }


        }

        private String invokeAnularReservaService(String myurl)
                throws IOException, JSONException {
            InputStream is = null;
            int length = 500;

            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("PUT");
                conn.setDoOutput(true);
                conn.setDoInput(true);

                JSONObject jObj = new JSONObject();
                jObj.put("ticket", mTicketId);
                byte[] outputInBytes = jObj.toString().getBytes("UTF-8");
                OutputStream os = conn.getOutputStream();
                os.write(outputInBytes);
                os.close();

                conn.connect();
                Log.d(TAG, "Connection opened");
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is: " + response);
                is = conn.getInputStream();

                // Convert the InputStream into a string
                return convertInputStreamToString(is, length);
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }


        public String convertInputStreamToString(InputStream stream, int length)
                throws IOException  {
            Reader reader;
            reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[length];
            reader.read(buffer);

            return new String(buffer);
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mReservaTask = null;

            //showProgress(false);
            if (success) {
                Toast.makeText(MainActivity.this, "El ticket fue anulado",
                        Toast.LENGTH_LONG).show();
                if (mMainActivityObj != null) {
                    mMainActivityObj.goToMisTickets();
                } else {
                    Fragment reservasFragment = new ReservasFragment();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.content_frame, reservasFragment, RESERVAS_FRAGMENT)
                            .commit();
                }
            } else {
                Toast.makeText(MainActivity.this, "Error al anular ticket",
                        Toast.LENGTH_LONG).show();
            }


        }

        @Override
        protected void onCancelled() {
            mReservaTask = null;
            //showProgress(false);
        }
    }


    public class MisTicketsTask extends AsyncTask<Void, Void, Boolean> {

        private String mUsername;
        private TicketList mTickets;
        private TicketsFragment.OnFragmentInteractionListener mListener;

        MisTicketsTask(String username, TicketsFragment.OnFragmentInteractionListener listener) {
            mUsername = username;
            mListener = listener;
        }


        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                if (!isCancelled()){
                    Log.d(TAG, "About to invoke service @ " + Params.RESTServiceURL +
                            "/tickets/mistickets");
                    String result = invokeReservaService(Params.RESTServiceURL +
                            "/tickets/mistickets");
                    Log.d(TAG, "Result: " + result);

                    JSONObject jObject = new JSONObject(result);

                    if (!jObject.has("status")) {
                        mTickets = TicketList.getTicketList(jObject);

                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }


            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
                return false;
            }


        }

        private String invokeReservaService(String myurl)
                throws IOException, JSONException {
            InputStream is = null;
            int length = 2000;

            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
                conn.setDoInput(true);

                JSONObject jObj = new JSONObject();
                jObj.put("user", mUsername);
                Log.d(TAG, jObj.toString());
                byte[] outputInBytes = jObj.toString().getBytes("UTF-8");
                OutputStream os = conn.getOutputStream();
                os.write(outputInBytes);
                os.close();

                conn.connect();
                Log.d(TAG, "Connection opened");
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is: " + response);
                is = conn.getInputStream();

                // Convert the InputStream into a string
                return convertInputStreamToString(is, length);
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }


        public String convertInputStreamToString(InputStream stream, int length)
                throws IOException  {
            Reader reader;
            reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[length];
            reader.read(buffer);
            return new String(buffer);
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mReservaTask = null;

            //showProgress(false);
            if (success) {
                Log.d(TAG, "Se leyeron " + mTickets.size() + " tickets.");
                DataHolder.setMisTickets(mTickets);
                TicketsFragment ticketsFragment = new TicketsFragment();
                ticketsFragment.setListener(mListener);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, ticketsFragment, TICKETS_FRAGMENT)
                        .commit();
            } else {
                Toast.makeText(MainActivity.this, "Error de transmisión de datos",
                        Toast.LENGTH_LONG).show();
            }


        }

        @Override
        protected void onCancelled() {
            mReservaTask = null;
            //showProgress(false);
        }
    }
}
