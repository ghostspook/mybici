package com.intelroad.mybici;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;

import com.intelroad.mybici.bizobjs.Ticket;
import com.intelroad.mybici.bizobjs.TicketList;

import java.util.List;

/**
 * Created by rafaelcastillo on 24/1/16.
 */
public class TicketListAdapter extends ArrayAdapter<Ticket> {
    private static final String TAG = TicketListAdapter.class.getSimpleName();
    private TicketList mTickets;
    private Context mContext;

    public TicketListAdapter(Context context, int resource, List<Ticket> tickets) {
        super(context, resource, tickets);
        mTickets = (TicketList) tickets;
        mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.ticket_row_layout, parent, false);
        TextView ticketNoView = (TextView) rowView.findViewById(R.id.ticket_id_textView);
        Log.d(TAG, "Ticket en posición " + position + ": " + mTickets.get(position).getId());
        ticketNoView.setText("Ticket No. " + mTickets.get(position).getId());
        TextView origenView = (TextView) rowView.findViewById(R.id.origen_plantilla);
        origenView.setText("Origen: " + mTickets.get(position).getEstacionOrigenNombre());
        TextView destinoView = (TextView) rowView.findViewById(R.id.destino_plantilla);
        destinoView.setText("Origen: " + mTickets.get(position).getEstacionDestinoNombre());
        ImageView image = (ImageView) rowView.findViewById(R.id.imageView3);
        Space spaceView = (Space) rowView.findViewById(R.id.blank_space_ticketrow);
        if (mTickets.get(position).getEstadoId() == 10) {
            image.setImageResource(R.drawable.mapmarker);
            final float scale = getContext().getResources().getDisplayMetrics().density;
            int pixels = (int) (40 * scale + 0.5f);
            ViewGroup.LayoutParams lp =  spaceView.getLayoutParams();
            lp.height = pixels;
            spaceView.setLayoutParams(lp);
        } else {
            image.setImageResource((R.drawable.marker_black));
        }

        return rowView;
    }
}
